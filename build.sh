#!/bin/bash

# check our arguments
if [ $# != 1 ]; then
    echo "Usage: $0 ./app"
    exit 1
fi

# set aside some space
mkdir -p ./build/linux

# some program globals
NODEKIT=~/bin/nw
NODEPAK=~/developer/nw.pak
ICUDTL=~/bin/icudtl.dat
CURDIR="`pwd`"
APPNAME="`basename \"$1\"`"

# the zip must have package.json in root directory
cd $1
# zip all files to nw archive
zip -r "$CURDIR/$APPNAME.nw" ./*
cd "$CURDIR"

# copy nw.pak from current build node-webkit
cp "$NODEPAK" ./nw.pak

# compilation to executable form
cat "$NODEKIT" "./${APPNAME}.nw" > "./build/linux/${APPNAME}"
chmod +x "./build/linux/${APPNAME}"

# move nw.pak to build folder
mv ./nw.pak ./build/linux/nw.pak
cp "$ICUDTL" ./build/linux/icudtl.dat

# remove temorary .nw
rm "./${APPNAME}.nw"

# display build complete notice
echo "Build Complete.  Your app is located at "
echo "          './build/linux/${APPNAME}'"
echo ""
echo " Don't forget to include dependencies during distribution!"
echo "           './build/linux/icudtl.dat'"
echo "           './build/linux/nw.pak'"
