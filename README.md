# nodekit-packager
A quick clean up of the node-webkit self-packing script, taken from the node-webkit wiki.

Usage
-----
Place the build.sh script *outside* your app directory or it will be included in the final package.  Recommended folder structure:

    ~/my-apps/
        myproject1/
        myproject2/
        build.sh
      
    *   build/linux/               * will be deleted and recreated by build script

**NOTE** You'll need to edit build.sh with the locations of your Node-Webkit installation!  You need 3 files from the package, icudtl.dat, nw.pak, and the program nw itself.  Just open build.sh and you'll see the variables with paths at the top of the file.

Execute the script

    cd ~/my-apps/
    ./build.sh myproject1
    
         .....
    
    ./build.sh myproject2
    
         .....
 
 Improvements welcome!  Thanks.
